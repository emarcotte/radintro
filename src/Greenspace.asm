.p816   ; 65816 processor
.i16    ; X/Y are 16 bits
.a8     ; A is 8 bits


SCREEN_WIDTH  = 256
SCREEN_TOP    = 10
SCREEN_BOTTOM = 224 - SCREEN_TOP

VIDEO_CONTROL = $2100 ; X---BBBB x = disable video video, B = brightness
BGMODE        = $2105 ; Controls size/colors of bgs.
                      ; Mode    # Colors for BG
                      ;          1   2   3   4
                      ; ======---=---=---=---=
                      ; 0        4   4   4   4
                      ; 1       16  16   4   -
                      ; 2       16  16   -   -
                      ; 3      256  16   -   -
                      ; 4      256   4   -   -
                      ; 5       16   4   -   -
                      ; 6       16   -   -   -
                      ; 7      256   -   -   -
                      ; 7EXTBG 256 128   -   -
BG1SC         = $2107 ; BG1 tilemap address register

CGRAM_INDEX   = $2121
CGRAM_WRITE   = $2122

VRAM_INDEX    = $2116
VRAM_INDEX_L  = $2116
VRAM_INDEX_H  = $2117
VRAM_WRITE    = $2118
VRAM_WRITE_L  = $2118
VRAM_WRITE_H  = $2119

OBSEL   = $2101
OAMADD  = $2102
OAMADDL = $2102
OAMADDH = $2103
OAMDATA = $2104

OAM_HFLIP = %01000000
OAM_VFLIP = %10000000
OAM_P1  = %00010000
OAM_P2  = %00100000
OAM_P3  = %00110000

SCREEN_MODE = $212c ; ---sdcba  s: sprites enable  d: BG4 enable  c: BG3 enable
                    ;           b: BG2 enable      a: BG1 enable
RDNMI    = $4210
NMITIMEN = $4200

.import __STACK_TOP

.segment "HEADER"
	.byte "FF"
	.byte "SNES"
	.byte 0, 0, 0, 0, 0, 0, 0
	.byte "RAD INTRO (C) 2017"

.segment "ROMINFO"
	.byte $30 ; Set ROM type to LoROM + FastROM
	.byte 0   ; No RAM
	.byte $07 ; 128K ROM
	.byte 79  ; Developer ID
	.byte $01 ; Region
	.byte $33
	.byte 1   ; Version!

	.word $AAAA ;chekcsum and compliment
	.word $5555

.segment "VECTORS"
	.addr 0
	.addr 0
	.addr EmptyHandler; Native COP
	.addr EmptyHandler; Native BRK
	.addr EmptyHandler; Native ABORT
	.addr VBlank
	.addr 0
	.addr EmptyHandler; Native IRQ
	.addr 0
	.addr 0
	.addr EmptyHandler; Emulated COP
	.addr 0
	.addr EmptyHandler; Emulated ABORT
	.addr EmptyHandler; Emulated NMI
	.addr reset       ; Emulated reset
	.addr 0           ; Emulated IRQ

; Define some cool variables n stuff.
.segment "BSS"
	fadeTicker:       .res 1
	fadeFunction:     .res 1
	screenBrightness: .res 1
	gridValues:       .res 9


.code

.A8
.I16
.export InitGame
.proc InitGame
	ldx #$09
@setGrid:
	stz gridValues,x
	dex
	bpl @setGrid

	lda #1
	sta fadeFunction

	stz fadeTicker
	lda #$0F
	sta screenBrightness
.endproc

.export PlayGame
.proc PlayGame
	wai

	rep #$30
	.A16
	.I16

	jsr ProcessFrame
	bra PlayGame
.endproc

.A16
.I16
.export ProcessFrame
.proc ProcessFrame
	rts
.endproc

.A8
.I16
.proc InitGFX
.export InitGFX
	; disable screen so we can load up vram outside of vblank
	lda #%10000000
	sta VIDEO_CONTROL

	stz NMITIMEN

	; Set up OAM sprite info
	lda #0
	sta OBSEL

	; load up palette #0
	lda #0
	sta CGRAM_INDEX
	lda #$EA
	sta CGRAM_WRITE
	lda #$0A
	sta CGRAM_WRITE

	; Load up palette #1
	lda #128
	sta CGRAM_INDEX

	ldx #$0
@palette_load:
		lda RadTextPalette, X
		sta CGRAM_WRITE
		inx
		cpx #16
		bne @palette_load

	; Load our tiles, big data mode (16bit A)
	rep #$30
	.A16
	lda #0
	sta VRAM_INDEX

	ldx #0
@tiles_load:
		lda RadText, X
		sta VRAM_WRITE
		inx
		inx
		cpx #RadText_End - RadText
		bcc @tiles_load

	; Back down to 8 bit for loading sprites into OAM
	sep #$20
	.A8

	; Set up the background mode
	lda #0
	sta BGMODE

	; Set up BG1-4
	lda #%00001000
	sta $2107
	lda #%00010000
	sta $2108
	lda #%00011000
	sta $2109
	lda #%00100000
	sta $210A

	lda #%00010001
	sta $210b
	lda #%00010001
	sta $210c

	stz OAMADDL
	stz OAMADDH

	; Clear all 128 sprites in oam
	ldx #128
@OamResetLoop:
		stz OAMDATA
		stz OAMDATA
		stz OAMDATA
		stz OAMDATA
		dex
		bpl @OamResetLoop

	; High Table
;	LDA	#128 / 4
;@OamResetHighLoop:
;		stz OAMDATA
;		dec
;		bpl @OamResetHighLoop

	; Set screen mode to show sprites
	lda %00010000
	sta SCREEN_MODE

	; Set up interupt flags
	; a-bc---d
	; a = Enable VBlank
	; b = Enable IRQ-H
	; c = Enable IRQ-V
	; d = Enable joypad auto reading
	lda #%10000001
	sta NMITIMEN

	; Set full brightness
	lda #$0F
	sta VIDEO_CONTROL

	rts
.endproc

.export FadeIn
.proc FadeIn
	lda screenBrightness
	cmp #$0F
	beq @end
	inc
	sta screenBrightness
	sta VIDEO_CONTROL

	; switch to fade out
@end:
	cmp #$0F
	bne @return
	lda #2
	sta fadeFunction
@return:
	rts
.endproc

.export FadeOut
.proc FadeOut
	lda screenBrightness
	cmp #0
	beq @end
	dec
	sta screenBrightness
	sta VIDEO_CONTROL

	; Switch to fade in
@end:
	cmp #0
	bne @return
	lda #1
	sta fadeFunction
@return:
	rts
.endproc

.export Fade
.proc Fade
	lda #0
	lda fadeTicker
	inc
	sta fadeTicker
	cmp #5 ; every 5 frames, fade
	bne @end
	stz fadeTicker
	lda fadeFunction
	cmp #1
	bne @try_out
	jsr FadeIn
	jmp @end
@try_out:
	cmp #2
	bne @end
	jsr FadeOut
@end:
	rts
.endproc

.export VBlank
.proc VBlank
	; Interupted by vblank, save processor state
	rep #$30
	pha
	phb
	phd
	phx
	phy

	phk
	plb

	rep #$30
	sep #$20
	.A8
	.I16

	; Reset NMI bit
	lda RDNMI

	ldy #0
	sty OAMADD

	jsr Fade

	ldy #0
	sty OAMADD

; Draw an 8x8 sprites that say "HELLO"
	lda #20
	sta OAMDATA ;x
	lda #40
	sta OAMDATA ;y
	lda #40 ; ascii h
	sta OAMDATA ; sprite index
	stz OAMDATA ; attributes

	lda #27
	sta OAMDATA ;x
	lda #40
	sta OAMDATA ;y
	lda #37 ; ascii e
	sta OAMDATA ; sprite index
	lda #(OAM_P3)
	sta OAMDATA ; attributes

	lda #34
	sta OAMDATA ;x
	lda #40
	sta OAMDATA ;y
	lda #44 ; ascii l
	sta OAMDATA ; sprite index
	stz OAMDATA ; attributes

	lda #41
	sta OAMDATA ;x
	lda #40
	sta OAMDATA ;y
	lda #44 ; ascii l
	sta OAMDATA ; sprite index
	stz OAMDATA ; attributes

	lda #48
	sta OAMDATA ;x
	lda #40
	sta OAMDATA ;y
	lda #47 ; ascii l
	sta OAMDATA ; sprite index
	stz OAMDATA ; attributes

	; Restore state
	rep #$30
	ply
	plx
	pld
	plb
	pla

	; End of the interupt
	rti
.endproc

EmptyHandler:
	rti

.proc reset
	; Switch to native mode in the 65816 is done by clearing the carry bit and
	; invoking the XCE instruction (copy carry bit into emulation bit.
	clc
	xce

	; Reset processor state
	; bit #4: 1 = 8 bit index register 0 = 16
	; bit #5: 1 = 8 bit accumulator    0 = 16
	; http://www.defence-force.org/computing/oric/coding/annexe_2/
	rep #$30
	sep #$20

	.A8
	.I16

	; Initialize a stack area ($1F80 - 1FFF)
	ldx #__STACK_TOP
	txs
	phk
	plb

	lda #0
	tcd

	jsr InitGFX
	jsr InitGame

	jmp PlayGame
.endproc

.rodata

RadText:
	.incbin "out/8x8_font.4bpp"
RadText_End:

RadTextPalette:
	.incbin "out/8x8_font.clr", 0, 16
