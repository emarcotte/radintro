# Rad Intro

This "game" can be built with https://github.com/cc65/cc65/.

Resources:

- Overall:      https://en.wikibooks.org/wiki/Super_NES_Programming
- Instructions: http://softpixel.com/~cwright/sianse/docs/65816NFO.HTM
- PCX2SNES:     https://raw.githubusercontent.com/gilligan/snesdev/master/tools/pcx2snes/pcx2snes.c
- Inspiration:  https://github.com/undisbeliever
- Other:        https://forums.nesdev.com/viewtopic.php?f=12&t=13662&sid=191d4c85ea71b49c66b2a05001cc8773
- ROM Validator http://ucon64.sourceforge.net/
