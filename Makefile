AS=ca65

.PHONY: all dirs clean continously

all: dirs out/Greenspace.sfc

out/Greenspace.sfc: out/Greenspace.o
	ld65 -v -vm -m $(@:.sfc=.memlog) -C lorom128.cfg $< -o $@ --dbgfile out/debug_info.txt
	cd out/ && ucon64 --snes -nhd --chk $(notdir $@)

out/%.o : src/%.asm
	$(AS) -I src/ $< -o $@

dirs: out/

out/:
	mkdir -p $@

run: out/Greenspace.sfc
	snes9x out/Greenspace.sfc

out/Greenspace.o: out/8x8_font.4bpp out/8x8_font.clr

out/8x8_font.4bpp out/8x8_font.clr: resources/8x8_font.pcx
	pcx2snes -n -s8 -c16 resources/8x8_font
	mv resources/8x8_font.clr out
	mv resources/8x8_font.pic out/8x8_font.4bpp

clean:
	$(RM) out/*

continously:
	while true; do make --silent; sleep 1; done
